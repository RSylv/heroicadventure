# Jeux d'aventure au travers de questions/reponses a choix multiple     
   
- Le jeu repose sur une base de tableau d'objet (les **step**) de l'histoire.
- Les objets sont indexés comme un array [0, 1, ...]
- La function "logique" du jeu recoit un index (au départ l'index 0), puis de manière récursive, cette function est relancé avec le nouvel index, qui n'est autre que la valeur "next" de l'objet ci-dessous.

(Exemple d'objet "**step**")
```
{
    story: "Votre histoire commence ici, dans votre village. <br> Vous etes sur le point de partir pour une longue aventure.. La quete sacrée du 'Preu de Pieux', un livre mythique ancestral qui aurait les pouvoirs de ne plus laisser les hommes et les femmes se faire manipuler facilement..",
    question: "Etes-vous prêt pour démarrer l'aventure",
    answers: [
        {
            text: "Non.. J'ai peur.",
            values: {
                // Remet les valeurs a zéro
                moral: -100, 
                life: -100,
                force: -100,
                next: -1  // Si next < 0 Alors le jeu lance la sequence de fin.
            }
        },
        {
            text: "JOUER",
            values: {
                // A zéro rien ne bouge, car ces valeurs sont additionnés 
                // aux valeurs déja existantes
                moral: 0,  
                life: 0,
                force: 0,
                next: 1
            }
        }
    ]
},
```  
  
### Règles des valeurs dans l'histoire :  
- Les valeurs de **life**, **moral** et **force**, sont comprises entre un minimum de 0 et un max de 100.  
  
- Si la vie tombe a zéro le personnage meurt
  
- Si le personnage rencontre un autre personnage et qu'il l'affronte il gagne ou perd le combat selon sa force.
Si il perd le combat, il perd de la vie 
  
- Le **moral** varie en fonction : 
  - des combats : Si le personnage combat et perd mais est proche du but le moral est moins affecté que si il en est loin.
  - des rencontres : Selon les échanges avec les autre personnages, donc selon les choix. Remonter le moral de certain/es peut faire baisser le notre.
   
<br>

... *a continuer* ..  
  
<br>
<hr>
<br>

#### GIT
> git clone ... 
  
Effacer les changements et revenir a l'etat initial du clone
> git reset || git reset --hard  