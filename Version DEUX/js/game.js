import { adventureSteps } from './story.js'
console.log(adventureSteps);

// Divs
const story = document.getElementById('story')
const question = document.getElementById('question')
const answers = document.getElementById('answers')
const endScreen = document.getElementById('endScreen')
// Bars
const lifeBar = document.getElementById('lifeBar')
const moralBar = document.getElementById('moralBar')
const forceBar = document.getElementById('forceBar')
const bars = document.querySelectorAll('.bar>div>div')

const restart = document.getElementById('restart')

let life = 0
let moral = 0
let force = 0
let step = 0
let userChoices = []

// Logique of the game
function storyGame(num = 0) {
    // If the num is -1 or under return the end of the game
    if (0 > num) {
        return thisIsTheEnd()
    }

    // Reinit the story step
    resetScreen()
    step++
    
    // Display messages (text, question, and answers)
    story.innerHTML = adventureSteps[num].story
    question.innerHTML = adventureSteps[num].question + ' ?'
    adventureSteps[num].answers.forEach(answer => {

        // Create a button foreach answer with the 'text' of the answer, and the 'next' answer.values
        const btn = document.createElement('button')
        btn.textContent = answer.text

        // Add event foreach button, according to the answer values
        btn.onclick = () => {
            let isFigthing = false
            // Si la vie à ajouter est négative (on a un combat, ou une blessure)
            if (Math.sign(answer.values.life) === -1) {
                calculatedLifeLost(answer.values.life)
                isFigthing = true
            }
            // Update the bars (life, moral and force), and check if player isAlive (return statement of the updateBars function)
            let isAlive = updateBars(answer.values, isFigthing)
            if (!isAlive) { return thisIsTheEnd() }

            // Si on demarre l'histoire, jouer le son
            if (num === 0) { startSound() }

            // If we arrived here, recurse/rerun the storyGame function with the 'next' answer.values
            storyGame(answer.values.next)
        }

        // Append each button to the answer list
        answers.append(btn)
    })
}

// Delete the answer of the screen
function resetScreen() {
    answers.innerHTML = ''
}

// Update bars values according to the asnwer values
function updateBars(values, isFigthing = false) {
    if (!isFigthing) {
        life = limit(life + values.life)
    }
    moral = limit(moral + values.moral)
    force = limit(force + values.force)

    lifeBar.style.height = life + '%'
    moralBar.style.height = moral + '%'
    forceBar.style.height = force + '%'

    return !(0 >= life)
}

// Set the damage recieved according to the current force
function calculatedLifeLost(value) {
    let coeff = force / 5
    life += value - coeff
    life = 0 > life ? 0 : life
    console.log('FIGHT', life)
}

// Limit a value between 0 et 100
function limit(val) {
    return val > 100 ? 100 : 0 > val ? 0 : val
}

// Update bars values according to the asnwer values
function thisIsTheEnd() {
    resetScreen()
    endSound()

    // Set a red background gradient
    endScreen.style.transition = '2s ease-out';
    endScreen.style.height = '100%'

    // Create and render a message
    let stepMessage = step > 1 ? `Vous avez perdu à la ${step} ème étapes !` : ''
    story.innerHTML = stepMessage + '<br> <img src="./img/deathFace.png" width=300>'
    question.innerHTML = "C'est bien ce qui me semblait, vous-n'êtes pas de taille.."

    // Display the restart button
    restart.classList.remove('hidden')

    // Animation empty bars
    setTimeout(() => {
        bars.forEach(bar => {
            bar.style.transition = '1s'
            bar.style.height = 0
        })
        restart.style.transform = 'scale(1)'
    }, 1000)
}

// Reinit the game
function reinitialize() {
    life = 0
    moral = 0
    force = 0
    step = 0
    userChoices = []
    
    // Reset the background gradient
    endScreen.style.transition = 'none';
    endScreen.style.height = '0'

    // hide restart btn
    restart.style.transform = 'scale(0) rotate(-1080deg)'
    restart.classList.add('hidden')

    // Reset the transition of the bars
    bars.forEach(bar => {
        bar.style.transition = '.4s'
    })

    // Set the start heigth of the bar
    lifeBar.style.height = life + '%'
    moralBar.style.height = moral + '%'
    forceBar.style.height = force + '%'
}

// Start the game
reinitialize()
storyGame()

restart.addEventListener('click', () => {
    reinitialize()
    storyGame()
})