const songs = [
    "andro-a-fabrice.mp3",
    "el-humahuaqueno.mp3",
    "bambuco-andino-tierradentro.mp3",
    "le-trio-joubran-masar.mp3",
    "an-ghoath-aeneas.mp3",
    "edo-lullaby-shakuhachi-shamisen-biwa-2-kotos-bells.mp3",
    "pipa-solo-hope.mp3",
    "russian-folk-music-the-bogatyr.mp3",
    "suite-deurope-centrale.mp3",
    "suite-de-la-renaissance-espagnole.mp3",
    "scarborough-fair-the-butterfly.mp3",
];
const path = "../sound/playlist/"
let num = 0

// Get an HTMLAudioElement according to the set src 
function getAudioElement(src) {
    const audioElement = document.createElement('audio');
    audioElement.src = src
    audioElement.volume = .125
    audioElement.currentTime = 0
    return audioElement
}

// Autoplay the songs playlist
const audio = getAudioElement(path + songs[num])
audio.autoplay = true;
audio.addEventListener('ended', () => {
    num++
    num = num > songs.length -1 ? 0 : num
    console.log('Next Song : ', songs[num])
    audio.src = path + songs[num]
    audio.currentTime = 0
    audio.play()
})

// Mute or Unmute the playlist
let mute = false
document.getElementById('mute').addEventListener('click', toggleSound)
function toggleSound(e) {
    mute = !mute
    if (mute) {
        e.target.src = "../img/icon/mute.png"
        audio.volume = 0
    } else {
        e.target.src = "../img/icon/unmute.png"
        audio.volume = .125
    }
}

// Play a sound at the start
const startGame = getAudioElement("../sound/storyBegin.wav")
function startSound() {
    audio.volume = 0
    endGame.volume = .1
    startGame.volume = .5
    startGame.play()
    setTimeout(() => {
        audio.volume = .125
    }, 3000)
}

// Play a sound on the endGame
const endGame = getAudioElement("../sound/defeat.ogg")
function endSound() {
    audio.volume = 0
    endGame.volume = .5
    endGame.play()
    setTimeout(() => {
        audio.volume = .125
    }, 7000)
}