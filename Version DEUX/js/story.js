/**
 * story: l'histoire qui s'afficher dans la div story
 * question: la question posé qui s'affiche dans la div question
 * answers: [
 *  text: Ce qui va s'afficher dans le bouton
 *  value: 
 *      - moral: de 0 a 100
 *      - life: de 0 a 100
 *      - force: de 0 a 100
 *      - next: l'index de la step suivante
 * ]
 */
export const adventureSteps = [
    // 0
    {
        story: "Votre histoire commence ici, dans votre village. <br> Vous êtes sur le point de partir pour une longue aventure.. La quête sacrée du 'Preu de Pieux', un livre mythique ancestral qui aurait les pouvoirs de ne plus laisser les hommes et les femmes se faire manipuler facilement..",
        question: "Etes-vous prêt pour démarrer l'aventure",
        answers: [
            {
                text: "Non.. J'ai peur.",
                values: {
                    moral: -100,
                    life: -100,
                    force: -100,
                    next: -1
                }
            },
            {
                text: "JOUER",
                values: {
                    moral: 50,
                    life: 50,
                    force: 50,
                    next: 1
                }
            }
        ]
    },
    // 1
    {
        story: "Ok c'est parti en voyage !",
        question: "Dire aurevoir a votre femme/homme",
        answers: [
            {
                text: "Non, pas la peine. Je me barre !",
                values: {
                    moral: -10,
                    life: 0,
                    force: 10,
                    next: 3
                }
            },
            {
                text: "Bien sûr !!",
                values: {
                    moral: 10,
                    life: 0,
                    force: 0,
                    next: 2
                }
            }
        ]
    },
    // 2
    {
        story: "vous: Aurevoir mon amour ! <br> conjoint: Aurevoir mon amour.. prends ceci avec toi. C'est un sandwich pour la route.",
        question: "Accepter le présent",
        answers: [
            {
                text: "Non, pas la peine. Je ne veux pas me surcharger.",
                values: {
                    moral: 0,
                    life: 0,
                    force: 20,
                    next: 3
                }
            },
            {
                text: "Bien sûr, merci mon amour.",
                values: {
                    moral: 20,
                    life: 10,
                    force: 20,
                    next: 3
                }
            }
        ]
    },
    // 3
    {
        story: "Vous voici en chemin vers votre destin.",
        question: "Voulez-vous siffloter en marchant",
        answers: [
            {
                text: "Surtout pas, je préfère méditer.",
                values: {
                    moral: 10,
                    life: 0,
                    force: 10,
                    next: 6
                }
            },
            {
                text: "Oh yess, I love rock'n'roll.",
                values: {
                    moral: 20,
                    life: 0,
                    force: 20,
                    next: 4
                }
            }
        ]
    },
    // 4
    {
        story: "Super tu rencontres Keith Richard qui te dit qui t'aime et qui voudrait t'accompagner",
        question: "Que voulez vous faire",
        answers: [
            {
                text: "Super qu'il vienne.",
                values: {
                    moral: 10,
                    life: 0,
                    force: 0,
                    next: 6
                }
            },
            {
                text: "Non je lui casse la gueule.",
                values: {
                    moral: -60,
                    life: -60,
                    force: -60,
                    next: 5
                }
            }
        ]
    },
    // 5
    {
        story: "Ils vous a detruit et vous a piquer le sandwich...",
        question: "On continue",
        answers: [
            {
                text: "Non c'est bon c'est trop dur pour moi.",
                values: {
                    moral: -100,
                    life: -100,
                    force: -100,
                    next: -1
                }
            },
            {
                text: "Bien sur !",
                values: {
                    moral: 0,
                    life: 0,
                    force: 0,
                    next: 6
                }
            }
        ]
    },
    // 6
    {
        story: "C'est reparti !!",
        question: "fff",
        answers: [
            {
                text: "ffff.",
                values: {
                    moral: -100,
                    life: -100,
                    force: -100,
                    next: -1
                }
            },
            {
                text: "fff",
                values: {
                    moral: 0,
                    life: 0,
                    force: 0,
                    next: 6
                }
            }
        ]
    },
]