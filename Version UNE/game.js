import { adventureSteps } from './story.js'

console.log(adventureSteps);

const lifeBar = document.getElementById('lifeBar')
const moralBar = document.getElementById('moralBar')
const forceBar = document.getElementById('forceBar')
const story = document.getElementById('story')
const question = document.getElementById('question')
const answers = document.getElementById('answers')

lifeBar.style.height = '50%'
moralBar.style.height = '50%'
forceBar.style.height = '50%'

let step = 0

// Logique of the game
function storyGame(num) {
    if (0 > num) {
        return thisIsTheEnd()        
    }
    resetScreen()
    step += 1
    story.innerHTML = adventureSteps[num].story
    question.innerHTML = adventureSteps[num].question + ' ?'
    adventureSteps[num].answers.forEach(answer => {
        const btn = document.createElement('button')
        btn.textContent = answer.text
        btn.dataset.next = answer.values.next
        btn.onclick = () => {
            let isAlive = updateBars(answer.values)
            if (!isAlive) {
                return thisIsTheEnd()        
            }
            storyGame(answer.values.next)
        }
        answers.append(btn)
    })
}

// Delete the answer of the screen
function resetScreen() {
    answers.innerHTML = ''
}

// Update bars values according to the asnwer values
function updateBars(values) {        
    let life = lifeBar.style.height.slice(0, lifeBar.style.height.length - 1) * 1 + values.life    
    life = life > 100 ? 100 : 0 > life ? 0 : life
    let moral = moralBar.style.height.slice(0, moralBar.style.height.length - 1) * 1 + values.moral
    moral = moral > 100 ? 100 : 0 > moral ? 0 : moral
    let force = forceBar.style.height.slice(0, forceBar.style.height.length - 1) * 1 + values.force
    force = force > 100 ? 100 : 0 > force ? 0 : force

    lifeBar.style.height = life + '%'
    moralBar.style.height = moral + '%'
    forceBar.style.height = force + '%'

    if (0 >= life) {
        return false    
    }
    return true
}

// Update bars values according to the asnwer values
function thisIsTheEnd() {
    resetScreen()
    let stepMessage = step > 1 ? `Vous avez perdu a la ${step} ème étape !` : ''
    story.innerHTML = stepMessage + '<br> <img src="./img/deathFace.png" width=300>'
    question.innerHTML = "C'est bien ce qui me semblait, vous-n'êtes pas de taille.."
}

// Start the game
storyGame(0)